# David O'Regan - Kneat interview

## Table of Contents

- [Tech Choices](#tech-choices)
- [Run Instructions](#run-instructions)
- [Folder Structure](#folder-structure)
- [Further Work](#further-worl)

## Tech Choices

- React
- Redux
- RXJS
- redux-observables
- Reactstrap
- Jest
- git flow

React and Redux are often used together as a project bootstrap. Leveraging these opinionated framework and state management systems lets us start with a solid base for the application. Next RXJS and redux-observables are added, following the duck module pattern. RXJS lets us make use of reactive paradims for async code execution and is very useful for user triggered API requests. Reacstrap is added for base styling. Jest is used as the test framework.

## Run Instructions

### Dev

`yarn` - Build assets
`yarn start` - Start dev server via webpack
`yarn build` - Build dist folder with production ready code
`yarn test` - Run test suite

### Build

## Folder Structure

The following is the base folder layout following the duck pattern for observable modules:

```
my-app/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    containers/
    components/
      atoms/
      molecules/
      organisms/
      templates/
    state/
      molecules/
        dashboard/
      utils/
      store.js
      epic.js
      reducer.js
    assets/
    utils/
```

### src/state/modules/dashboard/index.js

Our epic for starship trip calculation.

```js
const getStarShipSearchResultEpic = (action$, state$) =>
  action$
    .ofType(SEARCH_STARSHIP)
    .debounceTime(500)
    .map(({ inputValue }) => inputValue)
    .distinctUntilChanged()
    .map(inputValue => {
      const {
        entities: { starShips }
      } = state$.value[STATE_KEY]
      const searchResults = starShips.map(el => ({
        ...el,
        stopsNeeded:
          Math.round(inputValue / el.MGLT) === 1
            ? Math.round(inputValue / el.MGLT)
            : Math.round(inputValue / el.MGLT) - 1
      }))
      return {
        type: SEARCH_STARSHIPS_RESULTS,
        inputValue,
        searchResults
      }
    })
```

Our test epic for star ship stop calculation

```js
test('Test our calculations give correct results for Sentinel-class landing craft star ship', done => {
  const locationAction = {
    type: LOCATION_CHANGE
  }
  const action$ = ActionsObservable.of(locationAction)

  const result = searchStarShips(150)
  fetchDashboardOnInitialLoadEpic(
    action$,
    {},
    {
      ajax: dummyAjax
    }
  )
    .toArray()
    .subscribe(actualOutput => {
      expect(action.entities.starShips[1].stopsNeeded).toBe(1)
    })
})
```

## Further Work

- Make API call during getStarShipSearchResultEpic, instead of on page load
- Add more tests
- Add more responsive CSS
