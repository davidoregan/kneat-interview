import { combineEpics } from 'redux-observable'
import * as ajax from '../utils/ajax'
import { dashboardRootEpic } from './modules/dashboard'

export default (...args) =>
  combineEpics(dashboardRootEpic)(...args, {
    ajax
  })
