import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import dashboardReducer, {
  STATE_KEY as DASHBOARD_STATE_KEY
} from './modules/dashboard'

export default combineReducers({
  routing: routerReducer,
  [DASHBOARD_STATE_KEY]: dashboardReducer
})
