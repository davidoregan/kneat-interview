// @flow

import 'rxjs/add/operator/mapTo'
import 'rxjs/add/operator/switchMapTo'
import 'rxjs/add/operator/mergeMap'
import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/take'
import 'rxjs/add/operator/pluck'
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/retry'
import 'rxjs/add/operator/catch'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/distinctUntilChanged'
import { LOCATION_CHANGE } from 'react-router-redux'
import { combineEpics } from 'redux-observable'
import { mainDashboardPath } from '../../../containers/dashboard/urls'

const REQUEST_DASHBOARD_UPDATE = 'dashboard/REQUEST_DASHBOARD_UPDATE'
const DASHBOARD_UPDATE = 'dashboard/DASHBOARD_UPDATE'
const ERROR_UPDATING_DASHBOARD = 'dashboard/ERROR_UPDATING_DASHBOARD'
const SEARCH_STARSHIP = 'dashboard/SEARCH_STARSHIP'
const SEARCH_STARSHIPS_RESULTS = 'dashboard/SEARCH_STARSHIPS_RESULTS'

/**
 * Initial State for dashboard
 *
 */
const initialState = {
  entities: {
    // Starship array, straight from the API
    starShips: []
  },
  searchResults: [],
  inputValue: 0,
  isLoading: false
}

/**
 * STATE KEY
 */
export const STATE_KEY = 'dashboard'

/**
 * Reducer
 * @param state
 * @param action
 * @return {*}
 */
export default function reducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_DASHBOARD_UPDATE:
      return {
        ...state,
        isLoading: true
      }
    case DASHBOARD_UPDATE:
      return {
        ...state,
        isLoading: false,
        entities: {
          starShips: action.entities.results
        }
      }
    case ERROR_UPDATING_DASHBOARD:
      return {
        ...state,
        isLoading: false
      }
    case SEARCH_STARSHIPS_RESULTS:
      return {
        ...state,
        searchResults: action.searchResults,
        inputValue: action.inputValue
      }
    default:
      return state
  }
}
///
// Action Creators

/**
 *
 *
 * @export
 * @returns {Object}
 */
export function requestDashboardUpdate() {
  return {
    type: REQUEST_DASHBOARD_UPDATE
  }
}

/**
 *
 *
 * @export
 * @param {any} inputValue
 * @returns {Object}
 */
export function searchStarShips(inputValue) {
  return {
    type: SEARCH_STARSHIP,
    inputValue
  }
}

///
// Epics
/**
 * Keeps the current step in sync with the current router location
 * Looks for the step.path presence in the current location.pathname
 * @param action$
 * @param store
 * @return {Observable<any>}
 */
const updateDashboardEpic = (action$, store, { ajax }) =>
  action$
    .ofType(REQUEST_DASHBOARD_UPDATE)
    .switchMapTo(
      ajax
        .get('/starships')
        .pluck('response')
        .retry(2) // retry twice
        .catch(err => [])
    )
    .map(entities => ({
      type: DASHBOARD_UPDATE,
      entities
    }))

const fetchDashboardOnInitialLoadEpic = action$ =>
  action$
    .ofType(LOCATION_CHANGE)
    .map(({ payload }) => payload.pathname)
    .filter(pathname => pathname.indexOf(mainDashboardPath) > -1)
    .take(1)
    .mapTo({
      type: REQUEST_DASHBOARD_UPDATE
    })

const getStarShipSearchResultEpic = (action$, state$) =>
  action$
    .ofType(SEARCH_STARSHIP)
    .debounceTime(500)
    .map(({ inputValue }) => inputValue)
    .distinctUntilChanged()
    .map(inputValue => {
      const {
        entities: { starShips }
      } = state$.value[STATE_KEY]
      const searchResults = starShips.map(el => ({
        ...el,
        stopsNeeded:
          Math.round(inputValue / el.MGLT) === 1
            ? Math.round(inputValue / el.MGLT)
            : Math.round(inputValue / el.MGLT) - 1
      }))
      return {
        type: SEARCH_STARSHIPS_RESULTS,
        inputValue,
        searchResults
      }
    })

// Export a combined epic, less boilerplate in the rootEpic file
export const dashboardRootEpic = combineEpics(
  updateDashboardEpic,
  fetchDashboardOnInitialLoadEpic,
  getStarShipSearchResultEpic
)

/**
 * For testing purposes, we need individual epics and types
 * @type {{types: {REQUEST_DASHBOARD_UPDATE: string, DASHBOARD_UPDATE: string, epics: function(*, *): Observable<any>, fetchDashboardOnInitialLoadEpic: function(*): Observable<{type: string}>, updateDashboardEpic: function(*, *, {ajax?: *}): Observable<{}>}}}
 * @private
 */
export const __test = {
  types: {
    REQUEST_DASHBOARD_UPDATE,
    DASHBOARD_UPDATE
  },
  epics: {
    updateDashboardEpic,
    fetchDashboardOnInitialLoadEpic
  }
}
