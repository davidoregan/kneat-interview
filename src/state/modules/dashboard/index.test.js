import { Observable } from 'rxjs'
import 'rxjs/add/observable/throw'
import 'rxjs/add/observable/of'
import 'rxjs/add/observable/empty'
import 'rxjs/add/operator/toArray'
import { ActionsObservable } from 'redux-observable'
import { LOCATION_CHANGE } from 'react-router-redux'
import reducer, {
  searchStarShips,
  requestDashboardUpdate,
  __test
} from './index'
const {
  epics: { fetchDashboardOnInitialLoadEpic },
  types
} = __test

const dashboardTest = {
  entities: {
    starShips: [
      {
        MGLT: '40',
        cargo_capacity: '250000000',
        consumables: '6 years',
        cost_in_credits: '1143350000',
        created: '2014-12-15T12:31:42.547000Z',
        crew: '279144',
        edited: '2017-04-19T10:56:06.685592Z',
        films: [
          'https://swapi.co/api/films/2/',
          'https://swapi.co/api/films/3/'
        ],
        hyperdrive_rating: '2.0',
        length: '19000',
        manufacturer: 'Kuat Drive Yards, Fondor Shipyards',
        max_atmosphering_speed: 'n/a',
        model: 'Executor-class star dreadnought',
        name: 'Executor',
        passengers: '38000',
        pilots: [],
        starship_class: 'Star dreadnought',
        url: 'https://swapi.co/api/starships/15/'
      },
      {
        MGLT: '70',
        cargo_capacity: '180000',
        consumables: '1 month',
        cost_in_credits: '240000',
        created: '2014-12-10T15:48:00.586000Z',
        crew: '5',
        edited: '2014-12-22T17:35:44.431407Z',
        films: ['https://swapi.co/api/films/1/'],
        hyperdrive_rating: '1.0',
        length: '38',
        manufacturer: 'Sienar Fleet Systems, Cyngus Spaceworks',
        max_atmosphering_speed: '1000',
        model: 'Sentinel-class landing craft',
        name: 'Sentinel-class landing craft',
        passengers: '75',
        pilots: [],
        starship_class: 'landing craft',
        url: 'https://swapi.co/api/starships/5/'
      }
    ]
  }
}

const dummyAjax = () => Observable.empty()

describe('dashboard reduc module', () => {
  describe('Action creators', () => {
    test('requestDashboardUpdate', () => {
      const locationAction = {
        type: LOCATION_CHANGE
      }
      const action$ = ActionsObservable.of(locationAction)

      const result = requestDashboardUpdate()
      fetchDashboardOnInitialLoadEpic(
        action$,
        {},
        {
          ajax: dummyAjax
        }
      )
        .toArray()
        .subscribe(actualOutput => {
          expect(action.entities.starShips[0]).toBe(
            dashboardTest.entities.starShips[0]
          )
          done()
        })
    })
  })

  describe('Reducer', () => {
    let initialState
    beforeAll(() => {
      initialState = reducer(undefined, {
        type: 'not a type'
      })
    })
  })

  describe('Epics', () => {
    test('Test our calculations give correct results for Executor star ship', done => {
      const locationAction = {
        type: LOCATION_CHANGE
      }
      const action$ = ActionsObservable.of(locationAction)

      const result = searchStarShips(150)
      fetchDashboardOnInitialLoadEpic(
        action$,
        {},
        {
          ajax: dummyAjax
        }
      )
        .toArray()
        .subscribe(actualOutput => {
          expect(action.entities.starShips[0].stopsNeeded).toBe(3)
        })
    })

    test('Test our calculations give correct results for Sentinel-class landing craft star ship', done => {
      const locationAction = {
        type: LOCATION_CHANGE
      }
      const action$ = ActionsObservable.of(locationAction)

      const result = searchStarShips(150)
      fetchDashboardOnInitialLoadEpic(
        action$,
        {},
        {
          ajax: dummyAjax
        }
      )
        .toArray()
        .subscribe(actualOutput => {
          expect(action.entities.starShips[1].stopsNeeded).toBe(1)
        })
    })
  })
})
