// @flow

import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import { STATE_KEY, searchStarShips } from '../../state/modules/dashboard'

import {
  Button,
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Nav,
  NavItem,
  NavLink,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle
} from 'reactstrap'

import logo from '../../assets/img/logo.png'
import david from '../../assets/img/david.jpg'

import './dashboard.css'

class DashBoard extends Component {
  constructor(props) {
    super(props)
    this.handleSearch = this.handleSearch.bind(this)
    this.renderResults = this.renderResults.bind(this)
    this.state = {
      source: this.props.source,
      inputValue: this.props.inputValue,
      isLoading: false
    }
  }

  handleSearch(event) {
    const newValue = event.target.value
    this.props.searchStarShips(newValue)
    this.setState({
      inputValue: newValue
    })
  }

  renderResults() {
    return (
      <Fragment>
        {this.props.searchResults.map(starShip => (
          <Col md="4" key={starShip.name}>
            <div className="box">
              <Card>
                <CardImg
                  top
                  width="100%"
                  src="https://images3.alphacoders.com/115/115035.jpg"
                  alt="Ship Image"
                />
                <CardBody>
                  <CardTitle>{starShip.name}</CardTitle>
                  <CardSubtitle>
                    Refuel stops needed:{starShip.stopsNeeded}
                  </CardSubtitle>
                  <CardText>
                    <span>MGLT: {starShip.MGLT}</span>
                    <br />
                    <span>Model: {starShip.model}</span>
                    <br />
                    <span>Class: {starShip.starship_class}</span>
                    <br />
                    <span>Manufacturer: {starShip.manufacturer}</span>
                    <br />
                  </CardText>
                  <Button target="_blank" href={starShip.url}>
                    More
                  </Button>
                </CardBody>
              </Card>
            </div>
          </Col>
        ))}
      </Fragment>
    )
  }

  render() {
    return (
      <div>
        <Fragment>
          <section id="intro" className="intro">
            <div className="overlay" />
            <div className="content">
              <Container>
                <Row>
                  <Col lg="8" md="12">
                    <p className="italic">David O'Regan - Kneat Interview</p>
                    <h1>Why did Anakin Skywalker cross the road?</h1>
                    <p className="italic">To get to the Dark Side.</p>
                  </Col>
                </Row>
              </Container>
            </div>
          </section>
          <header className="header">
            <Nav className="navbar navbar-expand-lg">
              <Container>
                <a href="#intro" className="navbar-brand link-scroll">
                  <img
                    src={logo}
                    alt=""
                    className="img-fluid"
                    style={{ maxHeight: '3rem' }}
                  />
                </a>
                <Button
                  type="button"
                  data-toggle="collapse"
                  data-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
                  className="navbar-toggler navbar-toggler-right"
                >
                  <i className="fa fa-bars" />
                </Button>
                <div
                  id="navbarSupportedContent"
                  className="collapse navbar-collapse"
                >
                  <div className="navbar-nav ml-auto">
                    <NavItem className="nav-item">
                      <NavLink href="#intro" className="nav-link link-scroll">
                        Home
                      </NavLink>
                    </NavItem>
                    <NavItem className="nav-item">
                      <NavLink href="#about" className="nav-link link-scroll">
                        About{' '}
                      </NavLink>
                    </NavItem>
                    <NavItem className="nav-item">
                      <NavLink href="#search" className="nav-link link-scroll">
                        Calculate Stops
                      </NavLink>
                    </NavItem>
                    <NavItem className="nav-item">
                      <NavLink href="#results" className="nav-link link-scroll">
                        Results
                      </NavLink>
                    </NavItem>
                  </div>
                </div>
              </Container>
            </Nav>
          </header>
          <section id="about" className="text">
            <Container>
              <Row>
                <Col lg="6">
                  <h2 className="heading">About me</h2>
                  <p className="lead">I love the work I do. I always have.</p>
                  <p>
                    Working with the people who have passion and drive is
                    usually what helps me decide on which projects to take on. I
                    appreciate people that are driven by a vision, and my part
                    in their vision is to give it a medium of expression here on
                    the web.{' '}
                  </p>
                  <p>
                    I love web development in its whole and that spills into
                    making great UI's that adhere to the design principles I
                    respect. I strive for clean, easy to understand and
                    attractive UI's in any project I touch.{' '}
                  </p>
                  <p>I hope you will like my work.</p>
                </Col>
                <Col lg="5">
                  <p>
                    <img
                      src={david}
                      alt=""
                      className="img-fluid rounded-circle"
                    />
                  </p>
                </Col>
              </Row>
            </Container>
          </section>
          <section id="search" style={{ backgroundColor: '#eee' }}>
            <Container>
              <div className="row services">
                <Col lg="12">
                  <h2 className="heading">Starship Stops Needed Calculator</h2>
                  <Row>
                    <Col md="12">
                      <div className="box">
                        <div className="icon">
                          <i className="fa fa-desktop" />
                        </div>
                        <h4>Mega Lights Input</h4>
                        <p>
                          Enter the amount of Mega Lights you need to travel
                          i.e. 1000000
                        </p>
                        <Form>
                          <FormGroup>
                            <Label for="megaLights">ML</Label>
                            <Input
                              type="number"
                              name="megaLights"
                              id="megaLights"
                              placeholder="Enter Mega Lights..."
                              onChange={this.handleSearch}
                              value={this.state.inputValue}
                            />
                          </FormGroup>
                        </Form>
                      </div>
                    </Col>
                  </Row>
                </Col>
              </div>
            </Container>
          </section>
          <section id="results" className="gallery">
            <Container className="container clearfix">
              <Row>
                <Col lg="12">
                  <Row>
                    <Col md="12" lg="8">
                      <h2 className="heading">Results</h2>
                      <p>Lets see our starships performance...</p>
                    </Col>
                  </Row>
                  <Row>{this.renderResults()}</Row>
                </Col>
              </Row>
            </Container>
          </section>
        </Fragment>
        <footer style={{ backgroundColor: '#111' }}>
          <Container>
            <div className="row copyright">
              <Col md="6">
                <p className="mb-md-0 text-center text-md-left">
                  &copy;doregan
                </p>
              </Col>
              <Col md="6">
                <p className="credit mb-md-0 text-center text-md-right">
                  Made with love <a href="http://doregan.com">David O'Regan</a>
                </p>
              </Col>
            </div>
          </Container>
        </footer>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    starShips: state[STATE_KEY].entities.starShips,
    inputValue: state[STATE_KEY].inputValue,
    searchResults: state[STATE_KEY].searchResults
  }
}

const mapDispatchToProps = {
  searchStarShips
}

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)

export default enhance(DashBoard)
