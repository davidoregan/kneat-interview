import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import LazyLoad from '../../../components/atoms/LazyLoad'

const DashBoard = LazyLoad({
  loader: () => import('../../dashboard')
})
const NoMatch = LazyLoad({
  loader: () => import('../../nomatch')
})

class MainRoutes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/dashboard" component={DashBoard} />
        <Redirect to="/dashboard" />
        <Route component={NoMatch} />
      </Switch>
    )
  }
}

export default MainRoutes
