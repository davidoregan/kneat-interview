// @flow

import React, { Component } from 'react'
import MainRoutes from './routes'

class App extends Component {
  render() {
    return (
      <div className="App">
        <main>
          <MainRoutes />
        </main>
      </div>
    )
  }
}

export default App
