import React from 'react'
import { Jumbotron, Button } from 'reactstrap'

const NoMatch = props => {
  return (
    <div>
      <Jumbotron>
        <h1 className="display-3">404</h1>
        <p className="lead">Page not found!</p>
        <hr className="my-2" />
        <p className="lead">
          <Button color="primary">Go back</Button>
        </p>
      </Jumbotron>
    </div>
  )
}

export default NoMatch
