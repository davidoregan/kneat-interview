import Loadable from 'react-loadable'
import LoadingAsync from '../../molecules/LoadingAsync'

const LazyLoad = opts => {
  return Loadable({
    loading: LoadingAsync,
    ...opts
  })
}

export default LazyLoad
